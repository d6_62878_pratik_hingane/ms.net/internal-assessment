﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Emp
    {
        static int nextid = 0;
        private int _id;
        private string _name;
        private string _deptId;
        private double _basic;

        public Emp()
        {
            this._id = ++nextid;
        }

        

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string deptId
        {
            get { return _deptId; }
            set { _deptId = value; }
        }
        public double basic
        {
            get { return _basic;  }
            set { _basic = value; }
        }

        public void accept()
        {
            Console.WriteLine("Enter Name :");
            this.name = Console.ReadLine();
            Console.WriteLine("Enter deptId: ");
            this.deptId = Console.ReadLine();
            Console.WriteLine("Enter Basic :");
            this.basic = Convert.ToDouble(Console.ReadLine());
        }

        public override string ToString()
        {
            return ("Id : " + this._id + " " + "Name : " + this._name + "  " + "Dept Id: " + this._deptId+"  "+"Basic : "+this._basic);
        }


    }
}
