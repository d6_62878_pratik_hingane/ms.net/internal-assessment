﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class Worker:Emp
    {
        private int _hoursWorked;
        private double _hourlyRate;


        public double hourlyRate
        {
            get { return _hourlyRate; }
            set { _hourlyRate = value; }
        }

        public int hoursWorked
        {
            get { return _hoursWorked; }
            set { _hoursWorked = value; }
        }

        public void acceptworker()
        {
            this.accept();
            Console.WriteLine("Enter hours worked");
            this._hoursWorked = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter hourly rate");
            this._hourlyRate = Convert.ToDouble(Console.ReadLine());
        }
        public double netSalary()
        {
            return (this.basic + (this._hourlyRate * this._hoursWorked));
        }

        public override string ToString()

        {
            double netsalary = netSalary();
            return ("Id: " + this.id + ", " + "Name: " + this.name + ", " + "DeptId: " + this.deptId + ", " + "Basic: " + this.basic + ", " + "Hourly rate: " + this._hourlyRate + ", " + "Net salary: " + netsalary);
        }
    }
}
